/*
 * encoderM.h
 *
 *  Created on: 16 pa� 2016
 *      Author: Kamil
 */

#ifndef ENCODERM_H_
#define ENCODERM_H_

#include "stm32f10x.h"

void encoderInit(void);
int32_t getLeftEnc(void);
void resetLeftEnc(void);
int32_t getRightEnc(void);
void resetRightEnc(void);
#endif /* ENCODERM_H_ */
