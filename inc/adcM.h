/*
 * adcM.h
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */
#include "stm32f10x.h"

#ifndef ADCM_H_
#define ADCM_H_

#define ADC_CHANNELS	7
#define readLFSensor adc_value[5]
#define readRFSensor adc_value[6]
#define readLDSensor adc_value[0]
#define readRDSensor adc_value[1]
#define readBATadc	 adc_value[2]

extern uint16_t adc_value[ADC_CHANNELS];

void adcInit(void);
int adc_read(int channel);
float batteryLevel();
#endif /* ADCM_H_ */
