/*
 * timersM.h
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */

#ifndef TIMERSM_H_
#define TIMERSM_H_

#include "stm32f10x.h"

void timer1Init(void);
void Systick_Configuration(void);//initialize systick
void delay_ms(u32 nTime);
void delay_us(u32 nTime);
u32 micros(void);
u32 millis(void);
void elapseMicros(u32 targetTime, u32 oldt);
void elapseMillis(u32 targetTime, u32 oldt);

extern volatile u32 Millis;
extern volatile u32 Micros;

#define systemFrequency 72

#endif /* TIMERSM_H_ */
