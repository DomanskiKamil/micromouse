/*
 * ulnM.h
 *
 *  Created on: 16 pa� 2016
 *      Author: Kamil
 */

#ifndef ULNM_H_
#define ULNM_H_

#define LF_IR_ON GPIO_SetBits(GPIOB, GPIO_Pin_9)
#define LF_IR_OFF GPIO_ResetBits(GPIOB, GPIO_Pin_9)

#define RF_IR_ON GPIO_SetBits(GPIOC, GPIO_Pin_12)
#define RF_IR_OFF GPIO_ResetBits(GPIOC, GPIO_Pin_12)

#define DIAG_IR_ON GPIO_SetBits(GPIOB, GPIO_Pin_5|GPIO_Pin_8)
#define DIAG_IR_OFF GPIO_ResetBits(GPIOB, GPIO_Pin_5|GPIO_Pin_8)


void ulnInit(void);

#endif /* ULNM_H_ */
