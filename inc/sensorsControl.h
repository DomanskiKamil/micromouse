/*
 * sensorsControl.h
 *
 *  Created on: 29 pa� 2016
 *      Author: Kamil
 */

#ifndef SENSORSCONTROL_H_
#define SENSORSCONTROL_H_

#include "stm32f10x.h"

extern int32_t volMeter;
extern int32_t voltage;
extern int32_t LFSensor;
extern int32_t RFSensor;
extern int32_t LDSensor;
extern int32_t RDSensor;
extern int32_t Outz;
extern int32_t aSpeed;
extern int32_t angle;

void printAllSensorsVal(void);
void readSensors(void);
void readGyro(void);
void readVolMeter(void);
void lowBatCheck(void);

#endif /* SENSORSCONTROL_H_ */
