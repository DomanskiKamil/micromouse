/*
 * motorsM.h
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */

#ifndef MOTORSM_H_
#define MOTORSM_H_

void motorsInit(void);
void setRightMotor(int val);
void setLeftMotor(int val);

#endif /* MOTORSM_H_ */
