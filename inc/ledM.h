/*
 * ledM.h
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */

#ifndef LEDM_H_
#define LEDM_H_

#define LED4ON GPIO_SetBits(GPIOB, GPIO_Pin_15)
#define LED4OFF GPIO_ResetBits(GPIOB, GPIO_Pin_15)
#define LED3ON GPIO_SetBits(GPIOB, GPIO_Pin_14)
#define LED3OFF GPIO_ResetBits(GPIOB, GPIO_Pin_14)
void ledInit(void);

#endif /* LEDM_H_ */
