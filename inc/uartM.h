/*
 * uartM.h
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */

#ifndef UARTM_H_
#define UARTM_H_

void uartInit(void);
void send_char(char c);
int __io_putchar(int c);


#endif /* UARTM_H_ */
