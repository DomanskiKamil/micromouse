################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adcM.c \
../src/buttonM.c \
../src/encoderM.c \
../src/ledM.c \
../src/main.c \
../src/motorsM.c \
../src/sensorsControl.c \
../src/syscalls.c \
../src/system_stm32f10x.c \
../src/timersM.c \
../src/uartM.c \
../src/ulnM.c 

OBJS += \
./src/adcM.o \
./src/buttonM.o \
./src/encoderM.o \
./src/ledM.o \
./src/main.o \
./src/motorsM.o \
./src/sensorsControl.o \
./src/syscalls.o \
./src/system_stm32f10x.o \
./src/timersM.o \
./src/uartM.o \
./src/ulnM.o 

C_DEPS += \
./src/adcM.d \
./src/buttonM.d \
./src/encoderM.d \
./src/ledM.d \
./src/main.d \
./src/motorsM.d \
./src/sensorsControl.d \
./src/syscalls.d \
./src/system_stm32f10x.d \
./src/timersM.d \
./src/uartM.d \
./src/ulnM.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32F1 -DNUCLEO_F103RB -DSTM32F103RBTx -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F10X_MD -I"D:/workspace/micromouse/inc" -I"D:/workspace/micromouse/CMSIS/core" -I"D:/workspace/micromouse/CMSIS/device" -I"D:/workspace/micromouse/StdPeriph_Driver/inc" -I"D:/workspace/micromouse/Utilities/STM32F1xx-Nucleo" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


