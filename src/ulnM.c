/*
 * ulnM.c
 *
 *  Created on: 16 pa� 2016
 *      Author: Kamil
 */
#include "ulnM.h"
#include "stm32f10x.h"

ulnInit(){

	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_5|GPIO_Pin_8|GPIO_Pin_9;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &gpio);

	gpio.GPIO_Pin = GPIO_Pin_12;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &gpio);
	//GPIO_SetBits(GPIOB, GPIO_Pin_5|GPIO_Pin_8|GPIO_Pin_9);
//	GPIO_SetBits(GPIOC, GPIO_Pin_12);
	//GPIO_ResetBits(GPIOB, GPIO_Pin_5|GPIO_Pin_8|GPIO_Pin_9);
	//GPIO_SetBits(GPIOC, GPIO_Pin_10);
}
