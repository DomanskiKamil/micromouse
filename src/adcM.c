/*
 * adcM.c
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */
#include "adcM.h"
#include "stm32f10x.h"

uint16_t adc_value[ADC_CHANNELS];

int adc_read(int channel)
{
	//seven adc channels
	return adc_value[channel];

}
void printAllAdc()
{
	//seven adc channels
	 for (int i = 0; i < ADC_CHANNELS;i++){
		 float v = (float)adc_value[i] * 3.3f / 4096.0f;
		  printf("ADC%d = %d (%.3fV)    ",i, adc_value[i], v);
	 }
	 printf("\r\n");
}

adcInit(){

	  GPIO_InitTypeDef gpio;
	  DMA_InitTypeDef dma;
	  ADC_InitTypeDef adc;

	  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	  RCC_ADCCLKConfig(RCC_PCLK2_Div6);
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	  gpio.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2;
	  gpio.GPIO_Mode = GPIO_Mode_AIN;
	  GPIO_Init(GPIOA, &gpio);

	  gpio.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3;
	  gpio.GPIO_Mode = GPIO_Mode_AIN;
	  GPIO_Init(GPIOC, &gpio);

	  //GPIO_Pin_0|GPIO_Pin_1|

	  gpio.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;
	  gpio.GPIO_Mode = GPIO_Mode_AIN;
	  GPIO_Init(GPIOC, &gpio);

	  ////

	  DMA_StructInit(&dma);
	  dma.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
	  dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	  dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	  dma.DMA_MemoryBaseAddr = (uint32_t)adc_value;
	  dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	  dma.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	  dma.DMA_DIR = DMA_DIR_PeripheralSRC;
	  dma.DMA_BufferSize = ADC_CHANNELS;
	  dma.DMA_Mode = DMA_Mode_Circular;
	  DMA_Init(DMA1_Channel1, &dma);
	  DMA_Cmd(DMA1_Channel1, ENABLE);

	  ADC_StructInit(&adc);
	  adc.ADC_ScanConvMode = ENABLE;
	  adc.ADC_ContinuousConvMode = ENABLE;
	  adc.ADC_NbrOfChannel = ADC_CHANNELS;
	  adc.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	  ADC_Init(ADC1, &adc);

	  ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_239Cycles5);
	  ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 2, ADC_SampleTime_239Cycles5);
	  ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 3, ADC_SampleTime_239Cycles5);
	  ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 4, ADC_SampleTime_239Cycles5);
	  ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 5, ADC_SampleTime_239Cycles5);
	  ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 6, ADC_SampleTime_239Cycles5);
	  ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 7, ADC_SampleTime_239Cycles5);


	  ADC_DMACmd(ADC1, ENABLE);
	  ADC_Cmd(ADC1, ENABLE);

	  ADC_ResetCalibration(ADC1);
	  while (ADC_GetResetCalibrationStatus(ADC1));

	  ADC_StartCalibration(ADC1);
	  while(ADC_GetCalibrationStatus(ADC1));

	  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

float batteryLevel(){
	uint16_t adc = adc_value[2];
	float level = adc * 3.3f / 4096.0f;
	level = level*3.3f;
	return level;
}

