/*
 * sensorsControl.c
 *
 *  Created on: 29 pa� 2016
 *      Author: Kamil
 */
#include "sensorsControl.h"
#include "adcM.h"
#include "ulnM.h"
#include "ledM.h"
#include "motorsM.h"
#include "stm32f10x.h"
#include "timersM.h"

int32_t volMeter = 0;
int32_t voltage = 0;
int32_t LFSensor = 0;
int32_t RFSensor = 0;
int32_t LDSensor = 0;
int32_t RDSensor = 0;
int32_t Outz = 0;
int32_t aSpeed = 0;//angular velocity
int32_t angle = 0;

void printAllSensorsVal(){
	printf("LF= %d  ", LFSensor);
	printf("RF= %d  ", RFSensor);
	printf("LD= %d  ", LDSensor);
	printf("RD= %d  \r\n", RDSensor);
}

/*read IR sensors*/
void readSensors(void)
{
	u32 curt;
//read DC value

	LFSensor = readLFSensor;
	RFSensor = readRFSensor;
	LDSensor = readLDSensor;
	RDSensor = readRDSensor;

	curt = micros();

//left front sensor
	LF_IR_ON;
	elapseMicros(60,curt);
	LFSensor = readLFSensor - LFSensor;
	LF_IR_OFF;
	if(LFSensor < 0)//error check
		LFSensor = 0;
 	elapseMicros(140,curt);
//right front sensor
	RF_IR_ON;
	elapseMicros(200,curt);
	RFSensor = readRFSensor - RFSensor;
	RF_IR_OFF;
	if(RFSensor < 0)
		RFSensor = 0;
 	elapseMicros(280,curt);
//diagonal sensors
	DIAG_IR_ON;
	elapseMicros(340,curt);
	LDSensor = readLDSensor - LDSensor;
	RDSensor = readRDSensor - RDSensor;
	DIAG_IR_OFF;
	if(LDSensor < 0)
		LDSensor = 0;
	if(RDSensor < 0)
		RDSensor = 0;

	readVolMeter();

	//LFSensor = LFSensor*reflectionRate/1000;
	//RFSensor = RFSensor*reflectionRate/1000;
	//DLSensor = DLSensor*reflectionRate/1000;
	//DRSensor = DRSensor*reflectionRate/1000;

	//delay_us(80);
	//elapseMicros(500,curt);
}

void readVolMeter(void)
{          //3240 = 7.85V
	volMeter = readBATadc;//raw value
	//voltage = volMeter*809/3248;//actual voltage value  ex) 8.2V = 8200
}

void lowBatCheck(void)
{
  if(batteryLevel() < 7) //alert when battery Voltage lower than 7V
	{

		setLeftMotor(0);
		setRightMotor(0);
		LED4OFF;
		while(1)
		{
			LED3ON;
			delay_ms(200);


			LED3OFF;
			delay_ms(200);
		}
	}
}
