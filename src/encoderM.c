/*
 * encoderM.c
 *
 *  Created on: 16 pa� 2016
 *      Author: Kamil
 */
#include "encoderM.h"
#include "stm32f10x.h"

encoderInit(){

	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);

	GPIO_PinRemapConfig(GPIO_PartialRemap1_TIM2,ENABLE);

	//Right Encoder 15-A 3-B
	gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	gpio.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOA, &gpio);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12,TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_SetAutoreload(TIM3,0xffff);

	TIM_Cmd(TIM3, ENABLE);

	//Left Encoder PB6-A PB7-B
	GPIO_StructInit(&gpio);

	gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	gpio.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOB, &gpio);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	TIM_EncoderInterfaceConfig(TIM4, TIM_EncoderMode_TI12,TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_SetAutoreload(TIM4,0xffff);

	TIM_Cmd(TIM4, ENABLE);

}

int32_t getRightEnc(void){
	return TIM3->CNT;
}
void resetRightEnc(void){
	TIM3->CNT = 0;
}
int32_t getLeftEnc(void){
	return TIM4->CNT;
}
void resetLeftEnc(void){
	TIM4->CNT = 0;
}
