/*
 * motorsM.c
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */
#include "motorsM.h"
#include "timersM.h"
#include "stm32f10x.h"

void motorsInit(){

	//init Timers for L and R motor
	timer1Init();

	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	//init control pins for LM
	//RL1
	gpio.GPIO_Pin = GPIO_Pin_6;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &gpio);
	//RL2
	gpio.GPIO_Pin = GPIO_Pin_7;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &gpio);

	//init control pins for RM
	//RM1
	gpio.GPIO_Pin = GPIO_Pin_4;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &gpio);
	//RM2
	gpio.GPIO_Pin = GPIO_Pin_5;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &gpio);

	GPIO_ResetBits(GPIOC, GPIO_Pin_4);
	GPIO_ResetBits(GPIOC, GPIO_Pin_5);
	GPIO_ResetBits(GPIOC, GPIO_Pin_6);
	GPIO_ResetBits(GPIOC, GPIO_Pin_7);
}

void setRightMotor(int val){
	if(val >= 0){
		//TIM_SetCompare4(TIM1,500);
		TIM_SetCompare1(TIM1,val);
		GPIO_SetBits(GPIOC, GPIO_Pin_4);
		GPIO_ResetBits(GPIOC, GPIO_Pin_5);
	}
	else{
		TIM_SetCompare1(TIM1,abs(val));
		GPIO_SetBits(GPIOC, GPIO_Pin_5);
		GPIO_ResetBits(GPIOC, GPIO_Pin_4);
	}
}
void setLeftMotor(int val){
	if(val >= 0){

		TIM_SetCompare4(TIM1,val);
		GPIO_SetBits(GPIOC, GPIO_Pin_6);
		GPIO_ResetBits(GPIOC, GPIO_Pin_7);
	}
	else{
		TIM_SetCompare4(TIM1,abs(val));
		GPIO_SetBits(GPIOC, GPIO_Pin_7);
		GPIO_ResetBits(GPIOC, GPIO_Pin_6);
	}
}

