
#include "stm32f10x.h"
#include "uartM.h"
#include "timersM.h"
#include "ledM.h"
#include "motorsM.h"
#include "adcM.h"
#include "encoderM.h"
#include "buttonM.h"
#include <stdbool.h>

int32_t time;
int32_t motorvall =0;
int32_t motorvalr =0;
int32_t kp = 2;
int32_t ki = 1;
int32_t kd = -1;
int32_t el=0;
int32_t lastel=0;
int32_t esuml=0;
int32_t er=0;
int32_t laster=0;
int32_t esumr=0;
int32_t ul;
int32_t ur;
int32_t incl;
int32_t incr;
_Bool run = 0;

void SysTick_Handler()
{
	Millis++;
	//readSensors();
	if(Millis==1){
		lowBatCheck();
		resetRightEnc();
	}
	if(Millis<1000){
		if(run==0)
		{
			motorvall=500;
			setLeftMotor(motorvall);
			run=1;
		}
	printf("%d %d\r\n",getLeftEnc(),motorvall);
	resetLeftEnc();
	}
	else setLeftMotor(0);

}

void adjustPID(){
	incr = getRightEnc();
	if(incr>4000) incr=incr-65536;
	resetRightEnc();

	incl = getLeftEnc();
	if(incl>4000) incl=incl-65536;
	resetLeftEnc();

	er = motorvalr - incr;
	esumr +=er;
	if (esumr > 6000) {
		esumr = 6000;
	} else if (esumr < -6000) {
		esumr = -6000;
	}

	el = motorvall - incl;
	esuml +=el;
	if (esuml > 6000) {
		esuml = 6000;
	} else if (esuml < -6000) {
		esuml = -6000;
	}

	ur = (int)(kp*er + ki*esumr + kd*(er - laster));
	if(ur>1000)ur=1000;
	else if(ur<-1000) ur=-1000;

	ul = (int)(kp*el + ki*esuml + kd*(el - lastel));
	if(ul>1000)ul=1000;
		else if(ul<-1000) ul=-1000;

	if(el == 0 && er == 0){
		ul=0;
		ur=0;
	}
	setLeftMotor(ul);
	setRightMotor(ur);
	//printf("%d %d %d %d\r\n",incl,ul,incr,ur);
	laster=er;
	lastel=el;
}

int main(void)
{

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
	                         RCC_APB2Periph_GPIOB |
							 RCC_APB2Periph_GPIOC |
							 RCC_APB2Periph_AFIO, ENABLE);

	ledInit();
	motorsInit();
	uartInit();
	adcInit();
	LED4ON;
	ulnInit();
	encoderInit();

	resetRightEnc();

	buttonInit();
	Systick_Configuration();
    motorvall=0;

	printf("START\r\n");
	while(1){



	 }
}
