/*
 * timersM.c
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */
#include "timersM.h"
#include "stm32f10x.h"

volatile u32 Micros;
volatile u32 Millis;

void timer1Init(void){

	 RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);

	 GPIO_InitTypeDef gpio;
	 GPIO_StructInit(&gpio);

	 gpio.GPIO_Pin = GPIO_Pin_11;
	 gpio.GPIO_Speed = GPIO_Speed_50MHz;
	 gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_Init(GPIOA, &gpio);

	 gpio.GPIO_Pin = GPIO_Pin_8;
	 gpio.GPIO_Speed = GPIO_Speed_50MHz;
	 gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_Init(GPIOA, &gpio);

	 TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	 TIM_OCInitTypeDef  TIM_OCInitStructure;
	 TIM_BDTRInitTypeDef TIM_BDTRInitStructure;

	 /* Time Base configuration */
	 TIM_TimeBaseStructure.TIM_Prescaler = 64 - 1;
	 TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	 TIM_TimeBaseStructure.TIM_Period = 1000 - 1;;
	 TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	 TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	 TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	 /* Channel 1, 2 and 3 Configuration in PWM mode */
	 TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	 TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	 TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	 TIM_OCInitStructure.TIM_Pulse = 0;
	 TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	 TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	 TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	 TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
	 TIM_OC4Init(TIM1, &TIM_OCInitStructure);
	 TIM_OC1Init(TIM1, &TIM_OCInitStructure);

	 /* Automatic Output enable, Break, dead time and lock configuration*/
	 TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
	 TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
	 TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_OFF;
	 TIM_BDTRInitStructure.TIM_DeadTime = 0;
	 TIM_BDTRInitStructure.TIM_Break = TIM_Break_Disable;
	 TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;
	 TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;
	 TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);


	 /* TIM1 counter enable */
	 TIM_Cmd(TIM1, ENABLE);


	 /* Main Output Enable */
	 TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

void Systick_Configuration(void)
{
	SystemInit();
	SystemCoreClockUpdate();

	//systemFrequency = SystemCoreClock / 1000000;
	SysTick_Config (SystemCoreClock / 1000); //1ms per interrupt

	//set systick interrupt priority
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);	//4 bits for preemp priority 0 bit for sub priority
	NVIC_SetPriority(SysTick_IRQn, 0);

	Millis = 0;//reset Millis
}

u32 micros(void)
{
	Micros = Millis*1000 + 1000 - SysTick->VAL/systemFrequency;//=Millis*1000+(SystemCoreClock/1000-SysTick->VAL)/168;
	return Micros;
}

u32 millis(void)
{
	return Millis;
}

void delay_ms(u32 nTime)
{
	u32 curTime = Millis;
	while((nTime-(Millis-curTime)) > 0);
}

void delay_us(u32 nTime)
{
	u32 curTime = micros();
	while((nTime-(micros()-curTime)) > 0);
}

void elapseMicros(u32 targetTime, u32 oldt)
{
	while((micros()-oldt)<targetTime);
}


void elapseMillis(u32 targetTime, u32 oldt)
{
	while((millis()-oldt)<targetTime);
}
