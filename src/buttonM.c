/*
 * buttonM.c
 *
 *  Created on: 18 pa� 2016
 *      Author: Kamil
 */

#include "stm32f10x.h"
#include "buttonM.h"

buttonInit(){
	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_11;
	gpio.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOB, &gpio);

}
