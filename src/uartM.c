/*
 * uartM.c
 *
 *  Created on: 15 pa� 2016
 *      Author: Kamil
 */

#include "uartM.h"
#include "stm32f10x.h"

void uartInit(void){

	USART_InitTypeDef uart;
	GPIO_InitTypeDef gpio;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_9;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &gpio);

	USART_StructInit(&uart);
	uart.USART_BaudRate = 115200;
	USART_Init(USART1, &uart);

	USART_Cmd(USART1, ENABLE);
}
void send_char(char c)
{
 while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
 USART_SendData(USART1, c);
}

int __io_putchar(int c)
{
 send_char(c);
 return c;
}
